#!/usr/bin/env bash
# USAGE
# See `./redis --help`
#
# DESCRIPTION
# This is a utility script for managing a transient redis container. 
# The container is meant to be used in development and testing environments.
#
# CONFIGURATION
# You can configure the container through these environment variables:
#   REDIS_PORT
#   REDIS_IMAGE
#   REDIS_CONTAINER_NAME
#
# DEPENDENCIES
#   docker or podman (aliased to docker)

set -eo pipefail

REDIS_PORT="${REDIS_PORT:=6379}"
REDIS_CONTAINER_NAME="${REDIS_CONTAINER_NAME:=myapp-redis}"
REDIS_IMAGE="${REDIS_IMAGE:=redis:7-alpine}"

# Check that dependencies are installed
if ! [ -x "$(command -v docker)" ]; then
  echo "Error: docker is not installed" >&2
  exit 1
fi

ProgName=$(basename "$0")
subcommand=$1

RUNNING_CONTAINER=$(docker ps --filter "name=$REDIS_CONTAINER_NAME" --format '{{.ID}}')

sub_help() {
  echo "Usage: $ProgName <subcommand>"
  echo "Subcommands: "
  echo "  start   start the container"
  echo "  rm      kill and remove the container"
}

sub_start() {
   # if a redis container is running, print instructions to kill it and exit
  if [[ -n $RUNNING_CONTAINER ]]; then
    echo >&2 "there is a redis container already running, kill it with"
    echo >&2 "    $ProgName rm"
    exit 1
  fi

  # Launch Redis using Docker
  docker run \
    --publish "$REDIS_PORT:6379" \
    --detach \
    --name "$REDIS_CONTAINER_NAME" \
    "$REDIS_IMAGE"

  >&2 echo "Redis is ready to go!" 
}

sub_rm() {
  # kill the container if it is running
  if [[ -n $RUNNING_CONTAINER ]]; then
    docker kill "$RUNNING_CONTAINER"
  fi

  docker rm "$REDIS_CONTAINER_NAME"
  echo "Removed container '$REDIS_CONTAINER_NAME' successfully"
}

case $subcommand in
  "" | "-h" | "--help")
    sub_help
    ;;
  *)
    shift
    sub_"$subcommand" "$@"
    if [ $? = 127 ]; then
      echo "Error: '$subcommand' is not a known subcommand." >&2
      echo "   Run '$ProgName --help' for a list of known subcommands" >&2
      exit 1
    fi
esac
