/// Module for tailwind utils

export const colorVariants = {
	text: {
		error: "text-red-500",
		red: "text-red-500"
	},
	daisyButton: {
		primary: "btn-primary",
		secondary: "btn-secondary",
		accent: "btn-accent",
		info: "btn-info",
		success: "btn-success",
		warning: "btn-warning",
		error: "btn-error",
		ghost: "btn-ghost",
		link: "btn-link",
		outline: "btn-outline",
	},
	daisyAlert: {
		warning: "alert-warning",
		success: "alert-success",
		error: "alert-error",
	}
}

export const sizeVariants = {
	text: {
		xs: "text-xs",
		sm: "text-sm",
		md: "text-md",
		lg: "text-lg",
		xl: "text-xl",
		xl2: "text-2xl",
		xl3: "text-3xl",
		xl4: "text-4xl",
		xl5: "text-5xl",
		xl6: "text-6xl",
		xl7: "text-7xl",
		xl8: "text-8xl",
		xl9: "text-9xl",
	}
}

export const fontWeightVariants = {
	thin: "font-thin",
	extralight: "font-extralight",
	light: "font-light",
	normal: "font-normal",
	medium: "font-medium",
	semibold: "font-semibold",
	bold: "font-bold",
	extrabold: "font-extrabold",
	black: "font-black"
}
