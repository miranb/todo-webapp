use argon2::password_hash::SaltString;
use argon2::{Argon2, PasswordHasher};
use libtodo::domain::{Email, Task};
use libtodo::telemetry::{get_subscriber, init_subscriber};
use once_cell::sync::Lazy;
use rand::distributions::Alphanumeric;
use rand::{thread_rng, Rng};
use serde_json::json;
use sqlx::{Connection, Executor, PgConnection, PgPool};
use todo_front::config::{get_config, PostgresConfig};
use todo_front::startup::{get_pool, Application};
use uuid::Uuid;
use wiremock::MockServer;

// Ensure that the `tracing` stack is only initialised once using `once_cell`
static TRACING: Lazy<()> = Lazy::new(|| {
    let default_filter_level = "info".to_string();
    let subscriber_name = "test".to_string();

    // We cannot assign the output of `get_subscriber` to a variable based on
    // the value of `TEST_LOG` because the sink is part of the type returned by
    // `get_subscriber`, therefore they are not the same type. We could work
    // around it, but this is the most straight-forward way of moving forward.
    if std::env::var("TEST_LOG").is_ok() {
        let subscriber = get_subscriber(
            subscriber_name,
            default_filter_level,
            std::io::stdout,
        );
        init_subscriber(subscriber);
    } else {
        let subscriber = get_subscriber(
            subscriber_name,
            default_filter_level,
            std::io::sink,
        );
        init_subscriber(subscriber);
    }
});

pub struct TestApp {
    pub address: String,
    pub api_client: reqwest::Client,
    pub pool: PgPool,
    pub email_server: MockServer,
    pub port: u16,
    pub test_user: TestUser,
}

impl TestApp {
    /// Extract the confirmation links embedded in the request to the email
    /// API
    pub fn get_confirmation_links(
        &self,
        email_request: &wiremock::Request,
    ) -> ConfirmationLinks {
        let body: serde_json::Value =
            serde_json::from_slice(&email_request.body).unwrap();

        let get_link = |s: &str| {
            let links: Vec<_> = linkify::LinkFinder::new()
                .links(s)
                .filter(|l| *l.kind() == linkify::LinkKind::Url)
                .collect();
            assert_eq!(links.len(), 1);
            let raw_link = links[0].as_str().to_owned();
            let mut confirmation_link = reqwest::Url::parse(&raw_link).unwrap();
            // Let's make sure we don't call random APIs on the web
            assert_eq!(confirmation_link.host_str().unwrap(), "127.0.0.1");
            confirmation_link.set_port(Some(self.port)).unwrap();
            confirmation_link
        };

        let html = get_link(&body["HtmlBody"].as_str().unwrap());
        let plain_text = get_link(&body["TextBody"].as_str().unwrap());
        ConfirmationLinks { html, plain_text }
    }

    pub async fn get_login_html(&self) -> String {
        self.api_client
            .get(&format!("{}/login", &self.address))
            .send()
            .await
            .expect("Failed to execute request")
            .text()
            .await
            .unwrap()
    }

    pub async fn post_login<Body>(&self, body: &Body) -> reqwest::Response
    where
        Body: serde::Serialize,
    {
        self.api_client
            .post(&format!("{}/login", &self.address))
            .form(body)
            .send()
            .await
            .expect("Failed to execute request")
    }

    pub async fn post_signup<Body>(&self, body: Body) -> reqwest::Response
    where
        Body: serde::Serialize,
    {
        self.api_client
            .post(&format!("{}/signup", &self.address))
            .form(&body)
            .send()
            .await
            .expect("Failed to execute request")
    }

    pub async fn get_signup_html(&self) -> String {
        self.api_client
            .get(&format!("{}/signup", &self.address))
            .send()
            .await
            .expect("Failed to execute request")
            .text()
            .await
            .unwrap()
    }

    pub async fn post_tasks<Body>(&self, body: Body) -> reqwest::Response
    where
        Body: serde::Serialize,
    {
        self.api_client
            .post(&format!("{}/tasks", &self.address))
            .form(&body)
            .send()
            .await
            .expect("Failed to execute request")
    }

    pub async fn get_tasks_html(&self) -> String {
        self.api_client
            .get(&format!("{}/tasks", &self.address))
            .send()
            .await
            .expect("Failed to execute request")
            .text()
            .await
            .unwrap()
    }

    pub async fn get_tasks(&self) -> Vec<Task> {
        libtodo::core::tasks::get(self.test_user.user_id, &self.pool)
            .await
            .expect("Failed to get tasks")
    }

    /// Creates a task as the test user, keeps the session
    pub async fn create_task(&self) -> Uuid {
        self.post_login(&json!({
            "username": &self.test_user.username,
            "password": &self.test_user.password,
        }))
        .await
        .error_for_status()
        .expect("Failed to log in");

        self.post_tasks(&json!({
            "title": "My Awesome Title",
            "text": Uuid::new_v4().to_string(),
            "tags": "work, foo bar, xyz",
        }))
        .await
        .error_for_status()
        .expect("Failed to create task");

        let task_id = self
            .get_tasks()
            .await
            .first()
            .expect("No tasks were added")
            .task_id;

        task_id
    }

    pub async fn logout(&self) -> reqwest::Response {
        self.api_client
            .post(&format!("{}/logout", &self.address))
            .send()
            .await
            .expect("Failed to execute request")
    }

    pub async fn delete_task(&self, task_id: Uuid) -> reqwest::Response {
        self.api_client
            .post(format!("{}/tasks/{}/delete", &self.address, task_id))
            .send()
            .await
            .expect("Failed to execute request")
    }

    pub async fn toggle_status(&self, task_id: Uuid) -> reqwest::Response {
        self.api_client
            .post(format!("{}/tasks/{}/toggle-status", &self.address, task_id))
            .send()
            .await
            .expect("Failed to execute request")
    }

    pub async fn get_edit_task_page(&self, task_id: Uuid) -> reqwest::Response {
        self.api_client
            .get(format!("{}/tasks/{}/edit", self.address, task_id))
            .send()
            .await
            .expect("Failed to execute request")
    }

    pub async fn edit_task<Body>(
        &self,
        task_id: Uuid,
        body: Body,
    ) -> reqwest::Response
    where
        Body: serde::Serialize,
    {
        self.api_client
            .post(format!("{}/tasks/{}/edit", self.address, task_id))
            .form(&body)
            .send()
            .await
            .expect("Failed to execute request")
    }

    pub async fn change_email<Body>(&self, body: Body) -> reqwest::Response
    where
        Body: serde::Serialize,
    {
        self.api_client
            .post(format!("{}/settings/email", self.address))
            .form(&body)
            .send()
            .await
            .expect("Failed to execute request")
    }

    pub async fn get_settings_html(&self) -> String {
        self.api_client
            .get(format!("{}/settings", self.address))
            .send()
            .await
            .expect("Failed to execute request")
            .text()
            .await
            .unwrap()
    }

    pub async fn change_password<Body>(&self, body: Body) -> reqwest::Response
    where
        Body: serde::Serialize,
    {
        self.api_client
            .post(format!("{}/settings/password", self.address))
            .form(&body)
            .send()
            .await
            .expect("Failed to execute request")
    }

    pub async fn regenrate_api_key(&self) -> reqwest::Response {
        self.api_client
            .post(format!("{}/settings/api-key", self.address))
            .send()
            .await
            .expect("Failed to execute request")
    }

    pub async fn delete_account<Body>(&self, body: Body) -> reqwest::Response
    where
        Body: serde::Serialize,
    {
        self.api_client
            .post(format!("{}/settings/delete-account", self.address))
            .form(&body)
            .send()
            .await
            .expect("Failed to execute request")
    }

    pub async fn get_index_html(&self) -> String {
        self.api_client
            .get(&self.address)
            .send()
            .await
            .expect("Failed to execute request")
            .text()
            .await
            .unwrap()
    }

    pub async fn resend_confirmation_email(&self) -> reqwest::Response {
        self.api_client
            .post(format!(
                "{}/settings/resend-confirmation-email",
                &self.address
            ))
            .send()
            .await
            .expect("Failed to execute request")
    }
}

/// Confirmation links embedded in the request to the email API.
pub struct ConfirmationLinks {
    pub html: reqwest::Url,
    pub plain_text: reqwest::Url,
}

pub async fn spawn_app() -> TestApp {
    Lazy::force(&TRACING);

    let email_server = MockServer::start().await;

    // Randomise config to ensure test isolation
    let config = {
        let mut c = get_config().expect("Failed to read config.");
        c.postgres.db = Uuid::new_v4().to_string();
        c.port = 0;
        c.email_client.base_url =
            reqwest::Url::parse(&email_server.uri()).unwrap();
        c
    };

    configure_database(&config.postgres).await;

    let application = Application::build(config.clone())
        .await
        .expect("Failed to build application.");
    let application_port = application.port;

    let address = format!("http://127.0.0.1:{}", application_port);
    let _ = tokio::spawn(application.run_until_stopped());

    let api_client = reqwest::Client::builder()
        .redirect(reqwest::redirect::Policy::none())
        .cookie_store(true)
        .build()
        .unwrap();

    let test_app = TestApp {
        address,
        api_client,
        pool: get_pool(&config.postgres),
        email_server,
        port: application_port,
        test_user: TestUser::generate(),
    };
    test_app.test_user.store(&test_app.pool).await;

    test_app
}

pub async fn configure_database(config: &PostgresConfig) -> PgPool {
    let mut connection = PgConnection::connect_with(&config.without_db())
        .await
        .expect("Failed to connect to Postgres.");
    connection
        .execute(format!(r#"CREATE DATABASE "{}";"#, config.db).as_str())
        .await
        .expect("Failed to create database.");

    let pool = PgPool::connect_with(config.with_db())
        .await
        .expect("Failed to connect to Postgres.");
    libtodo::run_migrations(&pool).await;

    pool
}

pub struct TestUser {
    pub user_id: Uuid,
    pub username: String,
    pub email: Email,
    pub password: String,
    pub api_key: String,
}

impl TestUser {
    pub fn generate() -> Self {
        Self {
            user_id: Uuid::new_v4(),
            // TODO: generate random username every time
            username: "AKDJKAJSDLKAJDSKL".to_string(),
            // TODO: generate random email every time
            email: Email::parse("test@abc.com".into()).unwrap(),
            password: Uuid::new_v4().to_string(),
            api_key: generate_api_key(),
        }
    }

    async fn store(&self, pool: &PgPool) {
        let salt = SaltString::generate(&mut rand::thread_rng());
        let password_hash = Argon2::default()
            .hash_password(self.password.as_bytes(), &salt)
            .unwrap()
            .to_string();

        sqlx::query!(
            "INSERT INTO users (user_id, username, email, password_hash, status, api_key)
            VALUES ($1, $2, $3, $4, $5, $6)",
            self.user_id,
            self.username,
            self.email.as_ref(),
            password_hash,
            "pending_confirmation",
            self.api_key
        )
        .execute(pool)
        .await
        .expect("Failed to store test user.");
    }
}

/// Generate a random 32-characters-long case-sensitive api key
fn generate_api_key() -> String {
    let mut rng = thread_rng();
    std::iter::repeat_with(|| rng.sample(Alphanumeric))
        .map(char::from)
        .take(32)
        .collect()
}

pub fn assert_is_redirect_to(response: &reqwest::Response, location: &str) {
    assert_eq!(response.status().as_u16(), 303);
    assert_eq!(response.headers().get("Location").unwrap(), location);
}
