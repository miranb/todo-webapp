use crate::helpers::{assert_is_redirect_to, spawn_app};
use serde_json::json;

#[tokio::test]
async fn an_error_flash_message_is_set_on_failure() {
    // Arrange
    let app = spawn_app().await;

    // Act - Part 1 - Try to log in
    let body = json!({
        "username": "randomusername",
        "password": "randompassword"
    });
    let response = app.post_login(&body).await;

    // Assert
    assert_is_redirect_to(&response, "/login");

    // Act - Part 2 - Follow the redirect
    let html_page = app.get_login_html().await;
    assert!(html_page.contains(r#"<span>Authentication failed</span>"#));

    // Act - Part 3 - Reload the login page
    let html_page = app.get_login_html().await;
    assert!(!html_page.contains(r#"<span>Authentication failed</span>"#));
}

#[tokio::test]
async fn redirect_to_tasks_after_success() {
    // Arrange
    let app = spawn_app().await;

    // Act - Part 1 - Log in
    let body = json!({
        "username": &app.test_user.username,
        "password": &app.test_user.password
    });
    let response = app.post_login(&body).await;

    // Assert
    assert_is_redirect_to(&response, "/tasks");
}
