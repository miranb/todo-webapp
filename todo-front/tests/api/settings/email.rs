use crate::helpers::{assert_is_redirect_to, spawn_app};
use fake::faker::internet::en::SafeEmail;
use fake::Fake;
use serde_json::json;
use uuid::Uuid;
use wiremock::matchers::{method, path};
use wiremock::{Mock, ResponseTemplate};

#[tokio::test]
async fn redirects_anonymous_users_to_login() {
    // Arrange
    let app = spawn_app().await;

    // Act
    let response = app
        .change_email(json!({
            "new_email": SafeEmail().fake::<String>(),
            "password": &app.test_user.password,
        }))
        .await;

    // Assert
    assert_is_redirect_to(&response, "/login");
}

#[tokio::test]
async fn rejects_requests_with_wrong_password() {
    // Arrange
    let app = spawn_app().await;
    app.post_login(&json!({
        "username": &app.test_user.username,
        "password": &app.test_user.password,
    }))
    .await;

    // Act
    app.change_email(json!({
        "new_email": SafeEmail().fake::<String>(),
        "password": Uuid::new_v4().to_string(),
    }))
    .await;

    // Assert
    let saved = sqlx::query!(
        "SELECT email FROM users WHERE user_id=$1",
        &app.test_user.user_id
    )
    .fetch_one(&app.pool)
    .await
    .unwrap();
    assert_eq!(saved.email, app.test_user.email.as_ref());

    let settings_html = app.get_settings_html().await;
    assert!(
        settings_html.contains(r#"<span>Failed to confirm password.</span>"#)
    );
}

#[tokio::test]
async fn does_not_let_two_users_set_the_same_email() {
    // Arrange
    let app = spawn_app().await;

    // create new user
    let username = "testuser2";
    let email: String = SafeEmail().fake();
    let password = Uuid::new_v4().to_string();
    app.post_signup(&json!({
        "username": username,
        "email": email,
        "password": &password,
        "password_check": &password,
    }))
    .await;

    // log in as original testuser
    app.post_login(&json!({
        "username": &app.test_user.username,
        "password": &app.test_user.password,
    }))
    .await;

    // Act
    app.change_email(json!({
        "new_email": email,
        "password": &app.test_user.password,
    }))
    .await;

    // Assert
    let saved = sqlx::query!(
        "SELECT email FROM users WHERE user_id=$1",
        &app.test_user.user_id
    )
    .fetch_one(&app.pool)
    .await
    .unwrap();
    assert_eq!(saved.email, app.test_user.email.as_ref());

    let settings_html = app.get_settings_html().await;
    assert!(settings_html
        .contains(r#"<span>Another user is already using that email.</span>"#));
}

#[tokio::test]
async fn redirects_to_settings_upon_success() {
    // Arrange
    let app = spawn_app().await;
    let new_email: String = SafeEmail().fake();
    app.post_login(&json!({
        "username": &app.test_user.username,
        "password": &app.test_user.password,
    }))
    .await;

    // Act
    let response = app
        .change_email(json!({
            "new_email": &new_email,
            "password": &app.test_user.password,
        }))
        .await;

    // Assert
    assert_is_redirect_to(&response, "/settings");
}

#[tokio::test]
async fn changes_email_upon_success() {
    // Arrange
    let app = spawn_app().await;
    let new_email: String = SafeEmail().fake();
    app.post_login(&json!({
        "username": &app.test_user.username,
        "password": &app.test_user.password,
    }))
    .await;

    // Act
    app.change_email(&json!({
        "new_email": &new_email,
        "password": &app.test_user.password,
    }))
    .await;

    // Assert
    let saved_email = sqlx::query!(
        "SELECT email FROM users WHERE user_id=$1",
        &app.test_user.user_id
    )
    .fetch_one(&app.pool)
    .await
    .unwrap();

    assert_eq!(saved_email.email, new_email);
}

#[tokio::test]
async fn sends_a_confirmation_email_with_a_link() {
    // Arrange
    let app = spawn_app().await;
    let new_email: String = SafeEmail().fake();
    app.post_login(&json!({
        "username": &app.test_user.username,
        "password": &app.test_user.password,
    }))
    .await;

    Mock::given(path("/email"))
        .and(method("POST"))
        .respond_with(ResponseTemplate::new(200))
        .mount(&app.email_server)
        .await;

    // Act
    app.change_email(json!({
        "new_email": &new_email,
        "password": &app.test_user.password,
    }))
    .await;

    // Assert
    let email_request = &app.email_server.received_requests().await.unwrap()[0];
    let confirmation_links = app.get_confirmation_links(&email_request);

    assert_eq!(confirmation_links.html, confirmation_links.plain_text);
}

#[tokio::test]
async fn resets_status_upon_success() {
    // Arrange
    let app = spawn_app().await;
    let new_email: String = SafeEmail().fake();
    app.post_login(&json!({
        "username": &app.test_user.username,
        "password": &app.test_user.password,
    }))
    .await;

    // Act
    app.change_email(json!({
        "new_email": &new_email,
        "password": &app.test_user.password,
    }))
    .await;

    // Assert
    let saved = sqlx::query!(
        "SELECT status FROM users WHERE user_id=$1",
        &app.test_user.user_id
    )
    .fetch_one(&app.pool)
    .await
    .unwrap();

    assert_eq!(saved.status, "pending_confirmation");
}

#[tokio::test]
async fn sends_flash_message_upon_success() {
    // Arrange
    let app = spawn_app().await;
    let new_email: String = SafeEmail().fake();
    app.post_login(&json!({
        "username": &app.test_user.username,
        "password": &app.test_user.password,
    }))
    .await;

    Mock::given(path("/email"))
        .and(method("POST"))
        .respond_with(ResponseTemplate::new(200))
        .mount(&app.email_server)
        .await;

    // Act
    app.change_email(json!({
        "new_email": &new_email,
        "password": &app.test_user.password,
    }))
    .await;

    // Assert
    let settings_html = app.get_settings_html().await;
    assert!(settings_html.contains(
        r#"<span>Email updated successfully. Confirmation email sent.</span>"#
    ));
}
