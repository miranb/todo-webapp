use crate::helpers::{assert_is_redirect_to, spawn_app};
use claim::{assert_err, assert_ok};
use libtodo::auth::verify_password_hash;
use secrecy::Secret;
use serde_json::json;
use uuid::Uuid;

#[tokio::test]
async fn redirects_anonymous_users_to_login() {
    // Arrange
    let app = spawn_app().await;
    let new_password = Uuid::new_v4().to_string();

    // Act
    let response = app
        .change_password(json!({
            "current_password": &app.test_user.password,
            "new_password": &new_password,
            "new_password_check": &new_password,
        }))
        .await;

    // Assert
    assert_is_redirect_to(&response, "/login");
}

#[tokio::test]
async fn rejects_requests_with_wrong_old_password() {
    // Arrange
    let app = spawn_app().await;
    let new_password = Uuid::new_v4().to_string();
    app.post_login(&json!({
        "username": &app.test_user.username,
        "password": &app.test_user.password,
    }))
    .await;

    // Act
    app.change_password(json!({
        "current_password": Uuid::new_v4().to_string(),
        "new_password": &new_password,
        "new_password_check": &new_password,
    }))
    .await;

    // Assert
    let saved = sqlx::query!(
        "SELECT password_hash FROM users WHERE user_id=$1",
        &app.test_user.user_id
    )
    .fetch_one(&app.pool)
    .await
    .unwrap();

    let verify_result = verify_password_hash(
        Secret::new(saved.password_hash),
        Secret::new(new_password),
    );
    assert_err!(verify_result);

    let settings_html = app.get_settings_html().await;
    assert!(settings_html.contains(
        r#"<span>Failed to confirm old password.</span>"#
    ));
}

#[tokio::test]
async fn password_must_match_password_check() {
    // Arrange
    let app = spawn_app().await;
    let new_password = Uuid::new_v4().to_string();
    app.post_login(&json!({
        "username": &app.test_user.username,
        "password": &app.test_user.password,
    }))
    .await;

    // Act
    app.change_password(json!({
        "current_password": &app.test_user.password,
        "new_password": &new_password,
        "new_password_check": Uuid::new_v4().to_string(),
    }))
    .await;

    // Assert
    let saved = sqlx::query!(
        "SELECT password_hash FROM users WHERE user_id=$1",
        &app.test_user.user_id
    )
    .fetch_one(&app.pool)
    .await
    .unwrap();

    let verify_result = verify_password_hash(
        Secret::new(saved.password_hash),
        Secret::new(new_password),
    );
    assert_err!(verify_result);

    let settings_html = app.get_settings_html().await;
    assert!(
        settings_html.contains(
            r#"<span>New password must match new password check.</span>"#
        )
    );
}

#[tokio::test]
async fn warns_user_about_setting_same_password_again() {
    // Arrange
    let app = spawn_app().await;
    app.post_login(&json!({
        "username": &app.test_user.username,
        "password": &app.test_user.password,
    }))
    .await;

    // Act
    app.change_password(json!({
        "current_password": &app.test_user.password,
        "new_password": &app.test_user.password,
        "new_password_check": &app.test_user.password,
    }))
    .await;

    // Assert
    let settings_html = app.get_settings_html().await;
    assert!(settings_html.contains(
        r#"<span>New password must be different from old password.</span>"#
    ));
}

#[tokio::test]
async fn changes_password_upon_success() {
    // Arrange
    let app = spawn_app().await;
    let new_password = Uuid::new_v4().to_string();
    app.post_login(&json!({
        "username": &app.test_user.username,
        "password": &app.test_user.password,
    }))
    .await;

    // Act
    app.change_password(json!({
        "current_password": &app.test_user.password,
        "new_password": &new_password,
        "new_password_check": &new_password,
    }))
    .await;

    // Assert
    let saved = sqlx::query!(
        "SELECT password_hash FROM users WHERE user_id=$1",
        &app.test_user.user_id
    )
    .fetch_one(&app.pool)
    .await
    .unwrap();

    let verify_result = verify_password_hash(
        Secret::new(saved.password_hash),
        Secret::new(new_password),
    );
    assert_ok!(verify_result);
}

#[tokio::test]
async fn sets_flash_message_upon_success() {
    // Arrange
    let app = spawn_app().await;
    let new_password = Uuid::new_v4().to_string();
    app.post_login(&json!({
        "username": &app.test_user.username,
        "password": &app.test_user.password,
    }))
    .await;

    // Act
    let response = app
        .change_password(json!({
            "current_password": &app.test_user.password,
            "new_password": &new_password,
            "new_password_check": &new_password,
        }))
        .await;

    // Assert
    assert_is_redirect_to(&response, "/settings");

    let settings_html = app.get_settings_html().await;
    assert!(
        settings_html.contains(
            r#"<span>Password changed successfully.</span>"#
        )
    );
}
