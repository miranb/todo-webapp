use crate::helpers::{assert_is_redirect_to, spawn_app};
use claim::assert_none;
use serde_json::json;
use uuid::Uuid;

#[tokio::test]
async fn redirects_anonymous_users_to_login() {
    // Arrange
    let app = spawn_app().await;

    // Act
    let response = app
        .delete_account(json!({
            "username": app.test_user.username,
            "password": app.test_user.password,
        }))
        .await;

    // Assert
    assert_is_redirect_to(&response, "/login");
}

#[tokio::test]
async fn rejects_requests_with_wrong_username() {
    // Arrange
    let app = spawn_app().await;
    app.post_login(&json!({
        "username": &app.test_user.username,
        "password": &app.test_user.password,
    }))
    .await;

    // Act
    let response = app
        .delete_account(json!({
            "username": "someotherusername",
            "password": app.test_user.password,
        }))
        .await;

    // Assert
    assert_is_redirect_to(&response, "/settings");
    let settings_html = app.get_settings_html().await;
    assert!(
        settings_html.contains("<span>Failed to confirm credentials.</span>")
    );
}

#[tokio::test]
async fn rejects_requests_with_wrong_password() {
    // Arrange
    let app = spawn_app().await;
    app.post_login(&json!({
        "username": &app.test_user.username,
        "password": &app.test_user.password,
    }))
    .await;

    // Act
    let response = app
        .delete_account(json!({
            "username": app.test_user.username,
            "password": Uuid::new_v4().to_string(),
        }))
        .await;

    // Assert
    assert_is_redirect_to(&response, "/settings");
    let settings_html = app.get_settings_html().await;
    assert!(
        settings_html.contains("<span>Failed to confirm credentials.</span>")
    );
}

#[tokio::test]
async fn redirects_to_index_and_ends_session_upon_success() {
    // Arrange
    let app = spawn_app().await;
    app.post_login(&json!({
        "username": &app.test_user.username,
        "password": &app.test_user.password,
    }))
    .await;

    // Act
    let response = app
        .delete_account(json!({
            "username": app.test_user.username,
            "password": app.test_user.password,
        }))
        .await;

    // Assert
    assert_is_redirect_to(&response, "/");
    let index_html = app.get_index_html().await;
    assert!(index_html.contains("<span>Account deleted successfully.</span>"));

    // check that user has been logged out
    let response = app
        .api_client
        .get(format!("{}/settings", &app.address))
        .send()
        .await
        .expect("Failed to execute request");
    assert_is_redirect_to(&response, "/login");
}

#[tokio::test]
async fn deletes_account_successfully() {
    // Arrange
    let app = spawn_app().await;
    app.post_login(&json!({
        "username": &app.test_user.username,
        "password": &app.test_user.password,
    }))
    .await;

    // Act
    app.delete_account(json!({
        "username": app.test_user.username,
        "password": app.test_user.password,
    }))
    .await;

    // Assert
    let saved_users = sqlx::query!(
        "SELECT username FROM users WHERE user_id=$1",
        app.test_user.user_id,
    )
    .fetch_optional(&app.pool)
    .await
    .expect("Failed to execute query");
    assert_none!(saved_users);

    let saved_taks = sqlx::query!(
        "SELECT title FROM tasks WHERE user_id=$1",
        app.test_user.user_id,
    )
    .fetch_all(&app.pool)
    .await
    .expect("Failed to execute query");
    assert!(saved_taks.is_empty());

    let saved_tokens = sqlx::query!(
        "SELECT user_token FROM user_tokens WHERE user_id=$1",
        app.test_user.user_id,
    )
    .fetch_all(&app.pool)
    .await
    .expect("Failed to execute query");
    assert!(saved_tokens.is_empty());
}
