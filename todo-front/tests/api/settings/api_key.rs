use crate::helpers::{assert_is_redirect_to, spawn_app};
use serde_json::json;

#[tokio::test]
async fn redirects_anonymous_users_to_login() {
    // Arrange
    let app = spawn_app().await;

    // Act
    let response = app.regenrate_api_key().await;

    // Assert
    assert_is_redirect_to(&response, "/login");
}

#[tokio::test]
async fn regenerates_api_key_successfully() {
    // Arrange
    let app = spawn_app().await;
    app.post_login(&json!({
        "username": &app.test_user.username,
        "password": &app.test_user.password,
    }))
    .await;

    // Act
    app.regenrate_api_key().await;

    // Assert
    let saved = sqlx::query!(
        "SELECT api_key FROM users WHERE user_id=$1",
        &app.test_user.user_id
    )
    .fetch_one(&app.pool)
    .await
    .unwrap();

    assert_ne!(app.test_user.api_key, saved.api_key);
}

#[tokio::test]
async fn sets_flash_message_upon_success() {
    // Arrange
    let app = spawn_app().await;
    app.post_login(&json!({
        "username": &app.test_user.username,
        "password": &app.test_user.password,
    }))
    .await;

    // Act
    let response = app.regenrate_api_key().await;

    // Assert
    assert_is_redirect_to(&response, "/settings");
    let settigns_html = app.get_settings_html().await;
    assert!(settigns_html
        .contains(r#"<span>API key regenerated successfully.</span>"#));
}
