use actix_session::{Session, SessionExt, SessionGetError, SessionInsertError};
use actix_web::dev::Payload;
use actix_web::FromRequest;
use actix_web::HttpRequest;
use libtodo::domain::Username;
use std::future::{ready, Ready};
use uuid::Uuid;

pub struct TypedSession(Session);
impl TypedSession {
    const USER_ID_KEY: &'static str = "user_id";
    const USERNAME_KEY: &'static str = "username";

    pub fn renew(&self) {
        self.0.renew();
    }

    pub fn insert_user(
        &self,
        user_id: Uuid,
        username: Username,
    ) -> Result<(), SessionInsertError> {
        self.0.insert(Self::USER_ID_KEY, user_id)?;
        self.0.insert(Self::USERNAME_KEY, username)?;
        Ok(())
    }

    pub fn get_user_id(&self) -> Result<Option<Uuid>, SessionGetError> {
        self.0.get(Self::USER_ID_KEY)
    }

    pub fn get_username(&self) -> Result<Option<Username>, SessionGetError> {
        self.0.get(Self::USERNAME_KEY)
    }

    pub fn purge(self) {
        self.0.purge()
    }
}

impl FromRequest for TypedSession {
    type Error = <Session as FromRequest>::Error;
    type Future = Ready<Result<TypedSession, Self::Error>>;

    fn from_request(req: &HttpRequest, _payload: &mut Payload) -> Self::Future {
        ready(Ok(TypedSession(req.get_session())))
    }
}
