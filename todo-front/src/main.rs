use libtodo::telemetry::{get_subscriber, init_subscriber};
use todo_front::config::get_config;
use todo_front::startup::Application;

#[tokio::main]
async fn main() -> Result<(), anyhow::Error> {
    let subscriber =
        get_subscriber("todo".into(), "info".into(), std::io::stdout);
    init_subscriber(subscriber);

    let config = get_config().expect("Failed to read configuration.");
    let application = Application::build(config).await?;
    application.run_until_stopped().await?;

    Ok(())
}
