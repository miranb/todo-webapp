use crate::auth::UserId;
use actix_web::http::header::LOCATION;
use actix_web::{web, HttpResponse};
use actix_web_flash_messages::FlashMessage;
use libtodo::core;
use sqlx::PgPool;

pub async fn post(
    user_id: web::ReqData<UserId>,
    task_data: web::Form<core::tasks::PostTasksData>,
    pool: web::Data<PgPool>,
) -> HttpResponse {
    let user_id = user_id.into_inner();
    if let Err(e) = core::tasks::post(*user_id, task_data.0, &pool).await {
        FlashMessage::error(e.to_string()).send();
    }

    HttpResponse::SeeOther()
        .insert_header((LOCATION, "/tasks"))
        .finish()
}
