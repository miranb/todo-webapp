use crate::auth::UserId;
use crate::utils::ApplicationBaseUrl;
use actix_web::{web, HttpResponse, ResponseError};
use actix_web_flash_messages::FlashMessage;
use libtodo::core::settings::resend_confirmation_email;
use libtodo::email_client::EmailClient;
use libtodo::utils::error_chain_fmt;
use reqwest::header::LOCATION;
use sqlx::PgPool;
use std::fmt::Debug;

pub async fn post(
    user_id: web::ReqData<UserId>,
    pool: web::Data<PgPool>,
    email_client: web::Data<EmailClient>,
    base_url: web::Data<ApplicationBaseUrl>,
) -> Result<HttpResponse, ResendConfirmationEmailError> {
    let user_id = user_id.into_inner();

    resend_confirmation_email(
        *user_id,
        &pool,
        &email_client,
        base_url.0.as_str(),
    )
    .await?;

    FlashMessage::success("Sent new confirmation email.").send();
    Ok(HttpResponse::SeeOther()
        .insert_header((LOCATION, "/settings"))
        .finish())
}

#[derive(thiserror::Error)]
#[error(transparent)]
pub struct ResendConfirmationEmailError(#[from] anyhow::Error);

impl Debug for ResendConfirmationEmailError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        error_chain_fmt(self, f)
    }
}

impl ResponseError for ResendConfirmationEmailError {
    fn error_response(&self) -> HttpResponse {
        FlashMessage::error("Failed to send new confirmation email.").send();
        HttpResponse::SeeOther()
            .insert_header((LOCATION, "/settings"))
            .finish()
    }
}
