use actix_web::{web, HttpResponse};
use actix_web_flash_messages::FlashMessage;
use libtodo::core::settings::password::PatchPasswordData;
use libtodo::core::settings::password::patch as change_password;
use reqwest::header::LOCATION;
use sqlx::PgPool;
use crate::auth::UserId;

pub async fn post(
    user_id: web::ReqData<UserId>,
    password_data: web::Form<PatchPasswordData>,
    pool: web::Data<PgPool>,
) -> HttpResponse {
    let user_id = user_id.into_inner();

    match change_password(password_data.0, &pool, *user_id).await {
        Ok(_) => FlashMessage::success("Password changed successfully.").send(),
        Err(e) => FlashMessage::error(e.to_string()).send(),
    }

    HttpResponse::SeeOther()
        .insert_header((LOCATION, "/settings"))
        .finish()
}