use crate::session::TypedSession;
use actix_web::http::header::ContentType;
use actix_web::HttpResponse;
use actix_web_flash_messages::IncomingFlashMessages;
use libtodo::domain::Username;
use sailfish::TemplateOnce;

#[derive(sailfish::TemplateOnce)]
#[template(path = "index.stpl")]
struct IndexTemplate {
    pub username: Option<Username>,
    pub flash_messages: IncomingFlashMessages,
}

#[tracing::instrument(name = "index page", skip(session, flash_messages))]
pub async fn get(
    session: TypedSession,
    flash_messages: IncomingFlashMessages,
) -> HttpResponse {
    let username = session.get_username().unwrap_or(None);
    let body = IndexTemplate {
        username,
        flash_messages,
    }
    .render_once()
    .unwrap();

    HttpResponse::Ok()
        .content_type(ContentType::html())
        .body(body)
}
