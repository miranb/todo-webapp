//! This module is used only for database related functions shared between
//! multiple core functions or ones marked as public.
use crate::domain::{Email, Task, User, Username};
use crate::utils::generate_confirmation_token;
use anyhow::Context;
use secrecy::Secret;
use sqlx::{PgPool, Postgres, Transaction};
use uuid::Uuid;

#[tracing::instrument(
    name = "Getting a task from the database",
    skip(pool),
    err
)]
pub async fn get_task_from_id(
    task_id: Uuid,
    pool: &PgPool,
) -> Result<Task, anyhow::Error> {
    let task = sqlx::query_as!(
        Task,
        r#"
        SELECT task_id, text, status, created_at, user_id, title, tags
        FROM tasks
        WHERE task_id = $1
        "#,
        task_id
    )
    .fetch_one(pool)
    .await?;

    Ok(task)
}

#[tracing::instrument(name = "Get username", skip(pool), err)]
pub async fn get_username_from_id(
    user_id: Uuid,
    pool: &PgPool,
) -> Result<String, anyhow::Error> {
    let row = sqlx::query!(
        r#"
        SELECT username
        FROM users
        WHERE user_id = $1
        "#,
        user_id,
    )
    .fetch_one(pool)
    .await
    .context("Failed to perform a query to retreive the username.")?;
    Ok(row.username)
}

#[tracing::instrument(name = "Get user info from user id", skip(pool), err)]
pub async fn get_user_from_id(
    user_id: Uuid,
    pool: &PgPool,
) -> Result<User, anyhow::Error> {
    let row = sqlx::query!(
        r#"
        SELECT user_id, username, email, password_hash, status, created_at,
               api_key
        FROM users
        WHERE user_id = $1
        "#,
        user_id,
    )
    .fetch_one(pool)
    .await
    .context("Failed to perform a query to retreive the user's info.")?;

    let email = Email::parse(row.email)?;
    let username = Username::parse(row.username)?;

    Ok(User {
        user_id: row.user_id,
        username,
        email,
        password_hash: Secret::new(row.password_hash),
        api_key: row.api_key,
        status: row.status,
        created_at: row.created_at,
    })
}

#[tracing::instrument(
    name = "Gettings confirmation token for existing user",
    skip(pool),
    err
)]
pub async fn get_confirmation_token(
    user_id: Uuid,
    pool: &PgPool,
) -> Result<Option<String>, anyhow::Error> {
    let token = sqlx::query!(
        "SELECT user_token FROM user_tokens WHERE user_id=$1",
        user_id,
    )
    .fetch_optional(pool)
    .await?
    .map(|row| row.user_token);

    Ok(token)
}

#[tracing::instrument(
    name = "Removing user's info from database",
    skip(pool),
    err
)]
pub async fn delete_user(
    user_id: Uuid,
    pool: &PgPool,
) -> Result<(), anyhow::Error> {
    sqlx::query!("DELETE FROM users WHERE user_id=$1", user_id)
        .execute(pool)
        .await?;

    Ok(())
}

#[tracing::instrument(
    name = "Getting or creating confirmation token",
    skip(transaction),
    err
)]
/// Gets confirmation token if it already exists, and inserts new one if it
/// doesn't
pub async fn get_or_create_confirmation_token(
    transaction: &mut Transaction<'_, Postgres>,
    user_id: Uuid,
) -> Result<String, anyhow::Error> {
    let token = generate_confirmation_token();
    let row = sqlx::query!(
        r#"
        INSERT INTO user_tokens (user_token, user_id)
        VALUES ($1, $2)
        ON CONFLICT DO NOTHING
        RETURNING user_token
        "#,
        token,
        user_id
    )
    .fetch_one(transaction)
    .await
    .context("Failed to execute query")?;

    Ok(row.user_token)
}

#[tracing::instrument(
    name = "Creating confirmation token",
    skip(transaction),
    err
)]
pub async fn create_or_update_confirmation_token(
    transaction: &mut Transaction<'_, Postgres>,
    user_id: Uuid,
    token: &str,
) -> Result<(), anyhow::Error> {
    sqlx::query!(
        r#"
        INSERT INTO user_tokens (user_token, user_id)
        VALUES ($1, $2)
        ON CONFLICT (user_token) DO UPDATE SET user_token=$1
        "#,
        token,
        user_id
    )
    .execute(transaction)
    .await
    .context("Failed to execute query")?;

    Ok(())
}
