use crate::auth::{validate_credentials, AuthError, Credentials};
use sqlx::{PgPool, Postgres, Transaction};
use crate::core::users::send_confirmation_email;
use crate::email_client::EmailClient;
use crate::db;
use crate::domain::Email;
use crate::utils::{error_chain_fmt, generate_confirmation_token};
use anyhow::Context;
use secrecy::Secret;
use std::fmt::Debug;
use uuid::Uuid;

#[derive(serde::Deserialize)]
pub struct ChangeEmailData {
    pub new_email: String,
    pub password: Secret<String>,
}

#[tracing::instrument(
    name = "Changing a user's email",
    skip(email_data, pool, email_client, base_url),
    fields(
        new_email = %email_data.new_email
    ),
    err
)]
pub async fn change_email(
    email_data: ChangeEmailData,
    user_id: Uuid,
    pool: &PgPool,
    email_client: &EmailClient,
    base_url: &str,
) -> Result<(), ChangeEmailError> {
    let username = db::get_username_from_id(user_id, pool).await?;
    let credentials = Credentials {
        username,
        password: email_data.password,
    };
    validate_credentials(credentials, pool).await?;

    let token = generate_confirmation_token();
    let new_email = Email::parse(email_data.new_email)
        .map_err(|_| ChangeEmailError::ValidationError)?;

    let mut transaction = pool
        .begin()
        .await
        .context("Failed to acquire a Postgres connection from the pool.")?;

    db::create_or_update_confirmation_token(
        &mut transaction,
        user_id,
        &token
    ).await?;
    update_user_email(&new_email, user_id, &mut transaction).await?;

    transaction.commit().await.context("Failed to commit SQL transaction to update user's email.")?;

    send_confirmation_email(
        email_client,
        new_email,
        base_url,
        &token,
    )
    .await
    .map_err(|_| ChangeEmailError::SendEmailError)?;

    Ok(())
}

#[derive(thiserror::Error)]
pub enum ChangeEmailError {
    #[error("Email was not well formed.")]
    ValidationError,
    #[error("Failed to confirm password.")]
    AuthError(#[from] AuthError),
    #[error("Failed to send confirmation email.")]
    SendEmailError,
    #[error(transparent)]
    UnexpectedError(#[from] anyhow::Error),
}

impl Debug for ChangeEmailError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        error_chain_fmt(self, f)
    }
}

#[tracing::instrument(
    name = "Updating a user's email in the db",
    skip(transaction),
    err
)]
async fn update_user_email(
    new_email: &Email,
    user_id: Uuid,
    transaction: &mut Transaction<'_, Postgres>,
) -> Result<(), ChangeEmailError> {
    let resp = sqlx::query!(
        r#"
        UPDATE users
        SET email=$1
        WHERE user_id=$2
        "#,
        new_email.as_ref(),
        user_id
    )
    .execute(transaction)
    .await;

    match resp {
        Ok(_) => Ok(()),
        Err(sqlx::Error::Database(e))
            if e.constraint() == Some("users_email_key") =>
        {
            let e = anyhow::anyhow!("Another user is already using that email.");
            Err(ChangeEmailError::UnexpectedError(e))
        }
        Err(e) => Err(ChangeEmailError::UnexpectedError(e.into())),
    }
}

