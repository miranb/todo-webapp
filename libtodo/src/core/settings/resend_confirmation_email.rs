use crate::core::users::send_confirmation_email;
use crate::db;
use crate::domain::Email;
use crate::email_client::EmailClient;
use anyhow::Context;
use sqlx::PgPool;
use uuid::Uuid;

#[tracing::instrument(
    name = "Resending confirmation email",
    skip(pool, email_client, base_url),
    err
)]
pub async fn resend_confirmation_email(
    user_id: Uuid,
    pool: &PgPool,
    email_client: &EmailClient,
    base_url: &str,
) -> Result<(), anyhow::Error> {
    let mut transaction = pool
        .begin()
        .await
        .context("Failed to acquire a Postgres connection from the pool.")?;
    let confirmation_token =
        db::get_or_create_confirmation_token(&mut transaction, user_id)
            .await
            .context("Failed to get confirmation token")?;

    let user_email = get_email(user_id, pool).await?;

    send_confirmation_email(
        email_client,
        user_email,
        base_url,
        &confirmation_token,
    )
    .await
    .context("Failed to send confirmation email")?;

    Ok(())
}

#[tracing::instrument(
    name = "Getting a user's email from the db",
    skip(pool),
    err
)]
async fn get_email(
    user_id: Uuid,
    pool: &PgPool,
) -> Result<Email, anyhow::Error> {
    let row = sqlx::query!("SELECT email FROM users WHERE user_id=$1", user_id)
        .fetch_one(pool)
        .await?;

    let email = Email::parse(row.email).context(
        "Failed to parse email that was already in DB.\
            This shouldn't be possible.",
    )?;

    Ok(email)
}
