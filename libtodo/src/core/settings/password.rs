use crate::auth::{
    compute_password_hash, validate_credentials, AuthError, Credentials,
};
use crate::telemetry::spawn_blocking_with_tracing;
use crate::utils::error_chain_fmt;
use anyhow::Context;
use secrecy::{ExposeSecret, Secret};
use sqlx::PgPool;
use std::fmt::Debug;
use uuid::Uuid;

#[derive(serde::Deserialize)]
pub struct PatchPasswordData {
    current_password: Secret<String>,
    new_password: Secret<String>,
    new_password_check: Secret<String>,
}

#[tracing::instrument(
    name = "Updating a user's password",
    skip(body, pool),
    err
)]
pub async fn patch(
    body: PatchPasswordData,
    pool: &PgPool,
    user_id: Uuid,
) -> Result<(), PatchPasswordError> {
    if body.new_password.expose_secret()
        != body.new_password_check.expose_secret()
    {
        return Err(PatchPasswordError::ValidationError(anyhow::anyhow!(
            "new_password does not match new_password_check."
        )));
    }

    if body.current_password.expose_secret() ==
        body.new_password.expose_secret()
    {
        return Err(PatchPasswordError::UnexpectedError(anyhow::anyhow!(
            "New password must be different from old password."
        )));
    }

    let username = crate::db::get_username_from_id(user_id, pool)
        .await
        .map_err(PatchPasswordError::UnexpectedError)?;
    let credentials = Credentials {
        username,
        password: body.current_password,
    };
    validate_credentials(credentials, pool)
        .await
        .map_err(PatchPasswordError::from)?;

    change_password(user_id, body.new_password, pool)
        .await
        .map_err(PatchPasswordError::UnexpectedError)?;

    Ok(())
}

#[derive(thiserror::Error)]
pub enum PatchPasswordError {
    #[error("Failed to confirm old password.")]
    AuthError(anyhow::Error),
    #[error("New password must match new password check.")]
    ValidationError(anyhow::Error),
    #[error(transparent)]
    UnexpectedError(#[from] anyhow::Error),
}

impl Debug for PatchPasswordError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        error_chain_fmt(self, f)
    }
}

impl From<AuthError> for PatchPasswordError {
    fn from(error: AuthError) -> Self {
        match error {
            AuthError::InvalidCredentials(e) => {
                PatchPasswordError::AuthError(e)
            }
            AuthError::UnexpectedError(e) => {
                PatchPasswordError::UnexpectedError(e)
            }
        }
    }
}

#[tracing::instrument(name = "Change password", skip(password, pool), err)]
async fn change_password(
    user_id: Uuid,
    password: Secret<String>,
    pool: &PgPool,
) -> Result<(), anyhow::Error> {
    let password_hash =
        spawn_blocking_with_tracing(move || compute_password_hash(password))
            .await?
            .context("Failed to hash password")?;

    sqlx::query!(
        r#"
        UPDATE users
        SET password_hash = $1
        WHERE user_id = $2
        "#,
        password_hash.expose_secret(),
        user_id
    )
    .execute(pool)
    .await
    .context("Failed to change user's password in the database.")?;
    Ok(())
}
