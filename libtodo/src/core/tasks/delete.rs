use crate::db;
use crate::utils::error_chain_fmt;
use sqlx::PgPool;
use uuid::Uuid;

#[tracing::instrument(name = "Deleting a task", skip(pool), err)]
pub async fn delete(
    current_user_id: Uuid,
    task_id: Uuid,
    pool: &PgPool,
) -> Result<(), DeleteTaskError> {
    let task = db::get_task_from_id(task_id, pool)
        .await
        .map_err(DeleteTaskError::UnexpectedError)?;

    if task.user_id != current_user_id {
        return Err(DeleteTaskError::Unauthorized);
    }

    sqlx::query!(r#"DELETE FROM tasks WHERE task_id = $1 "#, task_id)
        .execute(pool)
        .await
        .map_err(anyhow::Error::from)
        .map_err(DeleteTaskError::UnexpectedError)?;

    Ok(())
}

#[derive(thiserror::Error)]
pub enum DeleteTaskError {
    #[error("You are not authorized to delete this task")]
    Unauthorized,
    #[error(transparent)]
    UnexpectedError(#[from] anyhow::Error),
}

impl std::fmt::Debug for DeleteTaskError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        error_chain_fmt(self, f)
    }
}
