use crate::domain::Task;
use crate::utils::error_chain_fmt;
use anyhow::Context;
use sqlx::PgPool;
use uuid::Uuid;

#[tracing::instrument(name = "Getting a user's tasks", skip(pool), err)]
pub async fn get(
    user_id: Uuid,
    pool: &PgPool,
) -> Result<Vec<Task>, GetTasksError> {
    let tasks = sqlx::query_as!(
        Task,
        r#"
        SELECT task_id, text, status, created_at, user_id, tags, title
        FROM tasks
        WHERE user_id = $1
        ORDER BY created_at DESC
        "#,
        user_id
    )
    .fetch_all(pool)
    .await
    .context("Failed to get tasks from database")?;
    Ok(tasks)
}

#[derive(thiserror::Error)]
#[error(transparent)]
pub struct GetTasksError(#[from] anyhow::Error);

impl std::fmt::Debug for GetTasksError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        error_chain_fmt(self, f)
    }
}
