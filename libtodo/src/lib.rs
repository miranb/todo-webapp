use sqlx::PgPool;

pub mod auth;
pub mod core;
pub mod db;
pub mod domain;
pub mod email_client;
pub mod telemetry;
pub mod utils;

/// Runs sqlx migrations on the db. Panics if migrations fail.
pub async fn run_migrations(pool: &PgPool) {
    sqlx::migrate!()
        .run(pool)
        .await
        .expect("Failed to run sqlx migrations")
}
