use crate::domain::{Email, Username};
use secrecy::Secret;

pub struct NewUser {
    pub username: Username,
    pub email: Email,
    pub password: Secret<String>,
}
