use chrono::NaiveDateTime;
use uuid::Uuid;

#[derive(sqlx::FromRow, serde::Serialize, serde::Deserialize)]
pub struct Task {
    pub user_id: Uuid,
    pub task_id: Uuid,
    pub title: String,
    pub text: Option<String>,
    pub status: String,
    pub tags: Vec<String>,
    pub created_at: NaiveDateTime,
}
