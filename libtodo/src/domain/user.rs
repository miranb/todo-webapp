use crate::domain::{Email, Username};
use chrono::NaiveDateTime;
use secrecy::Secret;
use uuid::Uuid;

pub struct User {
    pub user_id: Uuid,
    pub username: Username,
    pub email: Email,
    pub password_hash: Secret<String>,
    pub status: String,
    pub api_key: String,
    pub created_at: Option<NaiveDateTime>,
}
