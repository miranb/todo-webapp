ALTER TABLE tasks
DROP CONSTRAINT tasks_user_id_fkey,
ADD CONSTRAINT tasks_user_id_fkey
    FOREIGN KEY (user_id)
    REFERENCES  users(user_id)
    ON DELETE CASCADE;

ALTER TABLE user_tokens
DROP CONSTRAINT user_tokens_user_id_fkey,
ADD CONSTRAINT user_tokens_user_id_fkey
    FOREIGN KEY (user_id)
    REFERENCES  users(user_id)
    ON DELETE CASCADE;
