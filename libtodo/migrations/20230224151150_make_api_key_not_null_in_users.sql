BEGIN;
    UPDATE users
        SET api_key = SUBSTR(MD5(RANDOM()::text), 0, 32)
        WHERE api_key IS NULL;
    ALTER TABLE users ALTER COLUMN api_key SET NOT NULL;
COMMIT;
