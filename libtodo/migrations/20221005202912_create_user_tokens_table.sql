CREATE TABLE user_tokens (
    user_token TEXT NOT NULL,
    user_id UUID NOT NULL
        REFERENCES users (user_id),
    PRIMARY KEY (user_token)
);