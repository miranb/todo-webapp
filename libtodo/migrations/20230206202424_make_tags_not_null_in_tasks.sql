BEGIN;
    UPDATE tasks
        SET tags = '{}'
        WHERE tags IS NULL;
    ALTER TABLE tasks ALTER COLUMN tags SET NOT NULL;
COMMIT;