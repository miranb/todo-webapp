use crate::helpers::spawn_app;

#[tokio::test]
// We always return a 200 because if the user sends a invalid session user_id
// that means they weren't logged in anyway.
// In this case we're sending an empty user_id which should always be invalid.
async fn delete_session_returns_200_when_invalid_session_id_is_given() {
    // Arrange
    let app = spawn_app().await;

    // Act
    let response = app.delete_session().await;

    // Assert
    assert_eq!(response.status().as_u16(), 200);
}

#[tokio::test]
async fn delete_session_returns_200_when_session_is_deleted() {
    // Arrange
    let app = spawn_app().await;
    let body = serde_json::json!({
        "username": app.test_user.username,
        "password": app.test_user.password
    });

    app.post_api_session(&body).await;

    // Act
    let response = app.delete_session().await;

    // Assert
    assert_eq!(response.status().as_u16(), 200);
}
