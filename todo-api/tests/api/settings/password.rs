use crate::helpers::spawn_app;
use uuid::Uuid;

#[tokio::test]
async fn patch_update_password_returns_401_for_requests_missing_auth() {
    // Arrange
    let app = spawn_app().await;
    let body = serde_json::json!({
        "current_password": &app.test_user.password,
        "new_password": "newpass",
        "new_password_check": "newpass"
    });

    // Act
    let response = app.update_password(&body).await;

    // Assert
    assert_eq!(response.status().as_u16(), 401);
}

#[tokio::test]
async fn patch_update_password_returns_400_for_requests_missing_old_password() {
    // Arrange
    let app = spawn_app().await;
    let body = serde_json::json!({
        "username": &app.test_user.username,
        "password": &app.test_user.password,
    });

    // Act part 1 - create a session
    app.post_api_session(&body).await;

    // Arrange part 2
    let new_password = Uuid::new_v4().to_string();
    let body = serde_json::json!({
        "new_password": &new_password,
        "new_password_check": &new_password,
    });

    // Act part 2 - try to update the password
    let response = app.update_password(&body).await;

    // Assert
    assert_eq!(response.status().as_u16(), 400);
}

#[tokio::test]
async fn patch_update_password_returns_401_for_requests_with_bad_old_password()
{
    // Arrange
    let app = spawn_app().await;
    let body = serde_json::json!({
        "username": &app.test_user.username,
        "password": &app.test_user.password,
    });

    // Act part 1 - create a session
    app.post_api_session(&body).await;

    // Arrange part 2
    let new_password = Uuid::new_v4().to_string();
    let body = serde_json::json!({
        "current_password": "something else",
        "new_password": &new_password,
        "new_password_check": &new_password,
    });

    // Act part 2 - try to update the password
    let response = app.update_password(&body).await;

    // Assert
    assert_eq!(response.status().as_u16(), 401);
}

#[tokio::test]
async fn patch_update_password_returns_400_for_requests_missing_confirmation() {
    // Arrange
    let app = spawn_app().await;
    let body = serde_json::json!({
        "username": &app.test_user.username,
        "password": &app.test_user.password,
    });

    // Act part 1 - create a session
    app.post_api_session(&body).await;

    // Arrange part 2
    let body = serde_json::json!({
        "current_password": &app.test_user.password,
        "new_password": Uuid::new_v4().to_string(),
    });

    // Act part 2 - try to update the password
    let response = app.update_password(&body).await;

    // Assert
    assert_eq!(response.status().as_u16(), 400);
}

#[tokio::test]
async fn patch_update_password_returns_400_when_confirmation_does_not_match() {
    // Arrange
    let app = spawn_app().await;
    let body = serde_json::json!({
        "username": &app.test_user.username,
        "password": &app.test_user.password,
    });

    // Act part 1 - create a session
    app.post_api_session(&body).await;

    // Arrange part 2
    let body = serde_json::json!({
        "current_password": &app.test_user.password,
        "new_password": Uuid::new_v4().to_string(),
        "new_password_check": "something random",
    });

    // Act part 2 - try to update the password
    let response = app.update_password(&body).await;

    // Assert
    assert_eq!(response.status().as_u16(), 400);
}

#[tokio::test]
async fn patch_update_password_updates_password_for_valid_request() {
    // Arrange
    let app = spawn_app().await;
    let body = serde_json::json!({
        "username": &app.test_user.username,
        "password": &app.test_user.password,
    });

    // Act part 1 - create a session
    app.post_api_session(&body).await;

    // Arrange part 2
    let new_password = Uuid::new_v4().to_string();
    let body = serde_json::json!({
        "current_password": &app.test_user.password,
        "new_password": &new_password,
        "new_password_check": &new_password,
    });

    // Act part 2 - update the password
    let response = app.update_password(&body).await;
    assert_eq!(response.status().as_u16(), 200);

    // Arrange part 3
    app.delete_session().await;
    let body = serde_json::json!({
        "username": app.test_user.username,
        "password": &new_password,
    });

    // Act part 3 - create a new session with the new password
    let response = app.post_api_session(&body).await;

    // Assert
    assert_eq!(response.status().as_u16(), 200);
}
