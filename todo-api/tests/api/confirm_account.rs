use crate::helpers::spawn_app;
use wiremock::matchers::{method, path};
use wiremock::{Mock, ResponseTemplate};

#[tokio::test]
async fn confirmations_without_token_are_rejected_with_a_400() {
    // Arrange
    let app = spawn_app().await;

    // Act
    let response = app
        .api_client
        .get(&format!("{}/confirm-account", app.address,))
        .send()
        .await
        .unwrap();

    // Assert
    assert_eq!(response.status().as_u16(), 400);
}

#[tokio::test]
async fn the_link_returned_by_create_user_returns_a_200_when_called() {
    // Arrange
    let app = spawn_app().await;
    let body = serde_json::json!({
        "username": "testusername",
        "password": "mysupersecretpassword123+-",
        "email": "testuser@example.com",
    });

    Mock::given(path("/email"))
        .and(method("POST"))
        .respond_with(ResponseTemplate::new(200))
        .mount(&app.email_server)
        .await;

    app.post_users(&body).await;
    let email_request = &app.email_server.received_requests().await.unwrap()[0];
    let confirmation_links = app.get_confirmation_links(&email_request);

    // Act
    let response = reqwest::get(confirmation_links.html).await.unwrap();

    // Assert
    assert_eq!(response.status().as_u16(), 200);
}

#[tokio::test]
async fn clicking_on_the_confirmation_link_confirms_a_user() {
    // Arrange
    let app = spawn_app().await;
    let username = String::from("testusername");
    let body = serde_json::json!({
        "username": &username,
        "password": "mysupersecretpassword123+-",
        "email": "testuser@example.com",
    });

    Mock::given(path("/email"))
        .and(method("POST"))
        .respond_with(ResponseTemplate::new(200))
        .mount(&app.email_server)
        .await;

    app.post_users(&body).await;
    let email_request = &app.email_server.received_requests().await.unwrap()[0];
    let confirmation_links = app.get_confirmation_links(&email_request);

    // Act
    reqwest::get(confirmation_links.html)
        .await
        .unwrap()
        .error_for_status()
        .unwrap();

    // Assert
    let saved = sqlx::query!(
        "SELECT email, username, status FROM users WHERE username=$1",
        username
    )
    .fetch_one(&app.pool)
    .await
    .expect("Failed to fetch saved user.");

    assert_eq!(saved.email, "testuser@example.com");
    assert_eq!(saved.username, "testusername");
    assert_eq!(saved.status, "confirmed");
}
