use crate::helpers::spawn_app;
use libtodo::domain::Task;
use serde_json::json;

#[tokio::test]
async fn get_tasks_returns_401_for_requests_missing_auth() {
    // Arrange
    let app = spawn_app().await;

    // Act
    let response = app.get_tasks().await;

    // Assert
    assert_eq!(response.status().as_u16(), 401);
}

#[tokio::test]
async fn get_tasks_returns_tasks_correctly_upon_valid_request() {
    // Arrange
    let app = spawn_app().await;

    // Act part 1 - create a session
    let body = serde_json::json!({
        "username": &app.test_user.username,
        "password": &app.test_user.password
    });
    app.post_api_session(&body).await;

    // Act part 2 - create a couple new tasks
    app.post_tasks(&json!({ "title": "Yahoo" })).await;
    app.post_tasks(&json!({ "title": "adasd" })).await;
    app.post_tasks(&json!({ "title": "adjasjd" })).await;

    // Act part 3 - fetch the tasks
    let response = app.get_tasks().await;

    // Assert
    assert_eq!(response.status().as_u16(), 200);

    let body = response
        .json::<Vec<Task>>()
        .await
        .expect("Json was malformed");

    assert_eq!(body.len(), 3);
}
