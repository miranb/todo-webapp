use crate::helpers::spawn_app;
use libtodo::domain::Task;
use serde_json::json;
use uuid::Uuid;

#[tokio::test]
async fn patch_tasks_returns_401_for_requests_missing_auth() {
    // Arrange
    let app = spawn_app().await;
    let task_id = app.create_task().await;
    app.delete_session().await;

    // Act
    let body = json!({
        "text": "lkjasda",
        "status": "completed",
    });
    let response = app.patch_task(task_id, &body).await;

    // Assert
    assert_eq!(response.status().as_u16(), 401);
}

#[tokio::test]
async fn patch_tasks_returns_403_when_requested_to_delete_other_users_task() {
    // Arrange
    let app = spawn_app().await;

    wiremock::Mock::given(wiremock::matchers::path("/email"))
        .and(wiremock::matchers::method("POST"))
        .respond_with(wiremock::ResponseTemplate::new(200))
        .mount(&app.email_server)
        .await;

    // Act part 1 - create a task as the test and then log out
    let task_id = app.create_task().await;
    app.delete_session().await;

    // Act part 2 - create a new user
    let username = String::from("seconduser");
    let password = Uuid::new_v4().to_string();
    let body = json!({
        "username": &username,
        "password": &password,
        "email": "test@example.com"
    });

    app.post_users(&body)
        .await
        .error_for_status()
        .expect("Failed to create new user");

    // Act part 3 - log in as the new user and try to modify TestUser's task
    let body = json!({
        "username": &username,
        "password": &password,
    });
    app.post_api_session(&body)
        .await
        .error_for_status()
        .expect("Failed to log in");

    let body = json!({
        "text": "lkjasda",
        "status": "completed",
    });
    let response = app.patch_task(task_id, &body).await;

    // Assert
    assert_eq!(response.status().as_u16(), 403);
}

#[tokio::test]
async fn patch_tasks_successfully_modifies_task_upon_valid_request() {
    // Arrange
    let app = spawn_app().await;
    let task_id = app.create_task().await;

    // Act
    let body = json!({
        "text": "new text",
        "status": "completed",
    });
    let response = app.patch_task(task_id, &body).await;

    // Assert
    let task = app
        .get_tasks()
        .await
        .json::<Vec<Task>>()
        .await
        .expect("JSON was malformed");
    let task = task.first().expect("Task missing?");

    assert_eq!(response.status().as_u16(), 200);
    assert_eq!(task.text, Some("new text".to_string()));
    assert_eq!(task.status, "completed");
}
