use figment::providers::{Env, Format, Toml};
use figment::Figment;
use libtodo::domain::Email;
use secrecy::{ExposeSecret, Secret};
use sqlx::postgres::{PgConnectOptions, PgSslMode};

pub fn get_config() -> Result<Config, figment::Error> {
    Figment::new()
        .merge(Toml::file("todo-api.toml"))
        .merge(Env::raw())
        .extract()
}

#[derive(serde::Deserialize, Clone)]
pub struct Config {
    pub host: String,
    pub port: u16,
    pub base_url: String,
    pub postgres: PostgresConfig,
    pub email_client: EmailClientConfig,
    pub redis_uri: Secret<String>,
    pub hmac_secret: Secret<String>,
}

#[derive(serde::Deserialize, Clone)]
pub struct PostgresConfig {
    pub user: String,
    pub password: Secret<String>,
    pub host: String,
    pub port: u16,
    pub db: String,
    pub require_ssl: bool,
}

impl PostgresConfig {
    pub fn without_db(&self) -> PgConnectOptions {
        let ssl_mode = if self.require_ssl {
            PgSslMode::Require
        } else {
            PgSslMode::Prefer
        };
        PgConnectOptions::new()
            .host(&self.host)
            .username(&self.user)
            .password(self.password.expose_secret())
            .port(self.port)
            .ssl_mode(ssl_mode)
    }

    pub fn with_db(&self) -> PgConnectOptions {
        self.without_db().database(&self.db)
    }
}

#[derive(serde::Deserialize, Clone)]
pub struct EmailClientConfig {
    pub base_url: reqwest::Url,
    sender: String,
    pub auth_token: Secret<String>,
    timeout_millis: u64,
}

impl EmailClientConfig {
    pub fn sender(&self) -> Result<Email, anyhow::Error> {
        Email::parse(self.sender.clone())
    }

    pub fn timeout(&self) -> std::time::Duration {
        std::time::Duration::from_millis(self.timeout_millis)
    }
}
