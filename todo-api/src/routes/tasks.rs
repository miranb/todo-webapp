use crate::auth::UserId;
use actix_web::{web, HttpResponse};
use libtodo::core;
use libtodo::core::tasks::{DeleteTaskError, PatchTaskError, PostTasksError};
use sqlx::PgPool;
use uuid::Uuid;

pub async fn get(
    user_id: web::ReqData<UserId>,
    pool: web::Data<PgPool>,
) -> HttpResponse {
    let user_id = user_id.into_inner();
    match core::tasks::get(*user_id, &pool).await {
        Ok(tasks) => HttpResponse::Ok().json(tasks),
        Err(_) => HttpResponse::InternalServerError().finish(),
    }
}

pub async fn post(
    user_id: web::ReqData<UserId>,
    task_data: web::Json<core::tasks::PostTasksData>,
    pool: web::Data<PgPool>,
) -> HttpResponse {
    let user_id = user_id.into_inner();
    match core::tasks::post(*user_id, task_data.0, &pool).await {
        Ok(_) => HttpResponse::Ok().finish(),
        Err(PostTasksError::UnexpectedError(_)) => {
            HttpResponse::InternalServerError().finish()
        }
        Err(PostTasksError::ValidationError(_)) => {
            HttpResponse::BadRequest().finish()
        }
    }
}

pub async fn delete(
    current_user_id: web::ReqData<UserId>,
    task_id: web::Path<Uuid>,
    pool: web::Data<PgPool>,
) -> HttpResponse {
    let current_user_id = current_user_id.into_inner();
    match core::tasks::delete(*current_user_id, *task_id, &pool).await {
        Ok(_) => HttpResponse::Ok().finish(),
        Err(DeleteTaskError::Unauthorized) => {
            HttpResponse::Forbidden().finish()
        }
        Err(DeleteTaskError::UnexpectedError(_)) => {
            HttpResponse::InternalServerError().finish()
        }
    }
}

pub async fn patch(
    current_user_id: web::ReqData<UserId>,
    task_data: web::Json<core::tasks::PatchTaskData>,
    task_id: web::Path<Uuid>,
    pool: web::Data<PgPool>,
) -> HttpResponse {
    let current_user_id = current_user_id.into_inner();
    match core::tasks::patch(*current_user_id, task_data.0, *task_id, &pool)
        .await
    {
        Ok(_) => HttpResponse::Ok().finish(),
        Err(PatchTaskError::Unauthorized) => HttpResponse::Forbidden().finish(),
        Err(PatchTaskError::UnexpectedError(_)) => {
            HttpResponse::InternalServerError().finish()
        }
    }
}
