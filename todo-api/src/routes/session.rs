use crate::session::TypedSession;
use actix_session::SessionInsertError;
use actix_web::http::StatusCode;
use actix_web::{web, HttpResponse, ResponseError};
use libtodo::auth::{validate_credentials, AuthError, Credentials};
use libtodo::utils::error_chain_fmt;
use sqlx::PgPool;

#[tracing::instrument(
    name = "Create a new session",
    skip(credentials, pool, session),
    fields(username=tracing::field::Empty, user_id=tracing::field::Empty),
    err
)]
pub async fn post(
    credentials: web::Json<Credentials>,
    pool: web::Data<PgPool>,
    session: TypedSession,
) -> Result<HttpResponse, CreateSessionError> {
    tracing::Span::current()
        .record("username", &tracing::field::display(&credentials.username));

    let user_id = validate_credentials(credentials.0, &pool).await?;
    session.renew();
    session.insert_user_id(user_id)?;

    tracing::Span::current()
        .record("user_id", &tracing::field::display(&user_id));

    Ok(HttpResponse::Ok().finish())
}

#[derive(thiserror::Error)]
pub enum CreateSessionError {
    #[error("Authentication failed")]
    AuthError(#[source] anyhow::Error),
    #[error("Something went wrong")]
    UnexpectedError(#[from] anyhow::Error),
}

impl std::fmt::Debug for CreateSessionError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        error_chain_fmt(self, f)
    }
}

impl From<AuthError> for CreateSessionError {
    fn from(value: AuthError) -> Self {
        match value {
            AuthError::InvalidCredentials(e) => {
                CreateSessionError::AuthError(e)
            }
            AuthError::UnexpectedError(e) => {
                CreateSessionError::UnexpectedError(e)
            }
        }
    }
}

impl From<SessionInsertError> for CreateSessionError {
    fn from(value: SessionInsertError) -> Self {
        CreateSessionError::UnexpectedError(value.into())
    }
}

impl ResponseError for CreateSessionError {
    fn status_code(&self) -> reqwest::StatusCode {
        match self {
            CreateSessionError::AuthError(_) => StatusCode::UNAUTHORIZED,
            CreateSessionError::UnexpectedError(_) => {
                StatusCode::INTERNAL_SERVER_ERROR
            }
        }
    }
}

pub async fn delete(session: TypedSession) -> HttpResponse {
    session.purge();
    HttpResponse::Ok().finish()
}
