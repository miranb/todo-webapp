use crate::auth::reject_anonymous_users;
use crate::config::{Config, PostgresConfig};
use crate::routes;
use crate::utils::ApplicationBaseUrl;
use actix_session::storage::RedisSessionStore;
use actix_session::SessionMiddleware;
use actix_web::cookie::Key;
use actix_web::dev::Server;
use actix_web::{web, App, HttpServer};
use actix_web_lab::middleware::from_fn;
use libtodo::email_client::EmailClient;
use secrecy::{ExposeSecret, Secret};
use sqlx::postgres::PgPoolOptions;
use sqlx::PgPool;
use std::net::TcpListener;
use tracing_actix_web::TracingLogger;

pub struct Application {
    pub port: u16,
    server: Server,
}

impl Application {
    pub async fn build(config: Config) -> Result<Self, anyhow::Error> {
        let pool = get_pool(&config.postgres);

        let sender_email = config
            .email_client
            .sender()
            .expect("Invalid sender email address.");
        let timeout = config.email_client.timeout();
        let email_client = EmailClient::new(
            config.email_client.base_url.clone(),
            sender_email,
            config.email_client.auth_token.clone(),
            timeout,
        );

        let address = format!("{}:{}", config.host, config.port);
        let listener = TcpListener::bind(address)?;
        let port = listener.local_addr().unwrap().port();
        let server = run(
            listener,
            pool,
            email_client,
            config.base_url,
            config.hmac_secret,
            config.redis_uri,
        )
        .await?;

        Ok(Self { port, server })
    }

    pub async fn run_until_stopped(self) -> Result<(), std::io::Error> {
        self.server.await
    }
}

pub fn get_pool(config: &PostgresConfig) -> PgPool {
    PgPoolOptions::new()
        .acquire_timeout(std::time::Duration::from_secs(2))
        .connect_lazy_with(config.with_db())
}

async fn run(
    listener: TcpListener,
    pool: PgPool,
    email_client: EmailClient,
    base_url: String,
    hmac_secret: Secret<String>,
    redis_uri: Secret<String>,
) -> Result<Server, anyhow::Error> {
    let pool = web::Data::new(pool);
    let email_client = web::Data::new(email_client);
    let base_url = web::Data::new(ApplicationBaseUrl(base_url));
    let secret_key = Key::from(hmac_secret.expose_secret().as_bytes());
    let redis_store = RedisSessionStore::new(redis_uri.expose_secret()).await?;

    libtodo::run_migrations(&pool).await;

    let server = HttpServer::new(move || {
        App::new()
            .wrap(TracingLogger::default())
            .wrap(SessionMiddleware::new(
                redis_store.clone(),
                secret_key.clone(),
            ))
            .route("/health_check", web::get().to(routes::health_check))
            .route("/session", web::post().to(routes::session::post))
            .route("/session", web::delete().to(routes::session::delete))
            .service(
                web::scope("/tasks")
                    .wrap(from_fn(reject_anonymous_users))
                    .route("", web::get().to(routes::tasks::get))
                    .route("", web::post().to(routes::tasks::post))
                    .route(
                        "/{task_id}",
                        web::delete().to(routes::tasks::delete),
                    )
                    .route("/{task_id}", web::patch().to(routes::tasks::patch)),
            )
            .service(
                web::scope("/settings")
                    .wrap(from_fn(reject_anonymous_users))
                    .route(
                        "/password",
                        web::patch().to(routes::settings::password::patch),
                    ),
            )
            .app_data(pool.clone())
            .app_data(email_client.clone())
            .app_data(base_url.clone())
            .app_data(web::Data::new(HmacSecret(hmac_secret.clone())))
    })
    .listen(listener)?
    .run();
    Ok(server)
}

#[derive(Clone)]
pub struct HmacSecret(pub Secret<String>);
